import { combineReducers } from "redux";
import { urlReducer } from "./url";

const reducersMap = {
    currentUrl: urlReducer,
} as const;
type ReducersMapType = typeof reducersMap;

export const rootReducer = combineReducers(reducersMap);

export type RootState = ReturnType<typeof rootReducer>;
