export const validate = {
    email: (email?: string, allowEmpty = true) => {
        if (!email) {
            return allowEmpty;
        }
        const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return emailPattern.test(String(email).toLowerCase());
    },

    alphanumeric: (s: string) => {
        const alnumPattern = /^[a-zA-Z0-9_]*$/;
        return alnumPattern.test(s);
    },
} as const;
