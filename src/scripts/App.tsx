import { History } from "history";
import * as queryString from "query-string";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { match } from "react-router";
import "./App.scss";
import Footer from "./components/footer/Footer";
import Header from "./components/header/Header";
import Sidebar from "./components/sidebar/Sidebar";
import { updateCurrentUrl } from "./store/url";

const connector = connect(undefined, { updateCurrentUrl });

type AppProps = ConnectedProps<typeof connector> & {
    match: match;
    location: {
        search: string;
    };
    history: History;
};

class App extends React.Component<AppProps> {
    constructor(props: AppProps) {
        super(props);
        this.updateUrlparams(props);
    }

    componentDidUpdate() {
        this.updateUrlparams(this.props);
    }

    updateUrlparams = (curProps: AppProps) => {
        this.props.updateCurrentUrl({
            ...curProps.match,
            query: queryString.parse(curProps.location.search),
            queryString: curProps.location.search,
            history: this.props.history,
        });
    };

    render() {
        return (
            <div className="app">
                <div className="header-links">
                    <a target="_blank" href="http://www.umich.edu">
                        UNIVERSITY OF MICHIGAN
                    </a>
                    {" | "}
                    <a target="_blank" href="http://www.umich.edu/~pharmacy/">
                        COLLEGE OF PHARMACY
                    </a>
                    {" | "}
                    <a target="_blank" href="https://pharmacy.umich.edu/lomize-group">
                        LOMIZE GROUP
                    </a>
                </div>
                <Header />
                <div className="flex-container">
                    <Sidebar />
                    <div className="main-content">{this.props.children}</div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default connector(App);
