import "core-js/features/map";
import "core-js/features/set";
import "raf/polyfill";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { applyMiddleware, createStore } from "redux";
import { logger } from "redux-logger";
import App from "./App";
import About from "./components/about/About";
import Acknowledgments from "./components/acknowledgments/Acknowledgments";
import CellPMServer from "./components/cellpm_server/CellPMServer";
import Contact from "./components/contact/Contact";
import Help from "./components/help/Help";
import Home from "./components/home/Home";
import MembraneTypes from "./components/membrane_types/MembraneTypes";
import MoleculeViewer from "./components/molecule_viewer/MoleculeViewer";
import Error404 from "./Error404";
import { rootReducer } from "./store";

let store = createStore(rootReducer, applyMiddleware(logger));
const Main: React.FC = () => {
    return (
        <Provider store={store}>
            <Router>
                <Switch>
                    <Route
                        exact
                        path="/"
                        component={(props: any) => <App children={<Home />} {...props} />}
                    />
                    <Route
                        exact
                        path="/about"
                        component={(props: any) => <App children={<About />} {...props} />}
                    />
                    <Route
                        exact
                        path="/help"
                        component={(props: any) => <App children={<Help />} {...props} />}
                    />
                    <Route
                        exact
                        path="/contact"
                        component={(props: any) => <App children={<Contact />} {...props} />}
                    />

                    <Route
                        exact
                        path="/membrane_types/artificial_polarity_profiles"
                        component={(props: any) => (
                            <App
                                children={<MembraneTypes subsection={"artificial_profiles"} />}
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/membrane_types/biological_polarity_profiles"
                        component={(props: any) => (
                            <App
                                children={<MembraneTypes subsection={"biological_profiles"} />}
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/membrane_types/biological_descriptions"
                        component={(props: any) => (
                            <App
                                children={<MembraneTypes subsection={"biological_descriptions"} />}
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/molecule_viewer"
                        component={(props: any) => <App children={<MoleculeViewer />} {...props} />}
                    />
                    <Route
                        exact
                        path="/acknowledgments"
                        component={(props: any) => (
                            <App children={<Acknowledgments />} {...props} />
                        )}
                    />

                    <Route
                        exact
                        path="/cellpm_server"
                        component={(props: any) => <App children={<CellPMServer computationServer="memprot" />} {...props} />}
                    />
                    <Route
                        exact
                        path="/cellpm_server/submitted"
                        component={(props: any) => (
                            <App children={<CellPMServer computationServer="memprot" submitted />} {...props} />
                        )}
                    />
                    <Route
                        exact
                        path="/cellpm_server_cgopm"
                        component={(props: any) => <App children={<CellPMServer computationServer="cgopm" />} {...props} />}
                    />
                    <Route
                        exact
                        path="/cellpm_server_cgopm/submitted"
                        component={(props: any) => (
                            <App children={<CellPMServer computationServer="cgopm" submitted />} {...props} />
                        )}
                    />
                    <Route
                        exact
                        path="/cellpm_server_local"
                        component={(props: any) => <App children={<CellPMServer computationServer="local" />} {...props} />}
                    />
                    <Route
                        exact
                        path="/cellpm_server_local/submitted"
                        component={(props: any) => (
                            <App children={<CellPMServer computationServer="local" submitted />} {...props} />
                        )}
                    />

                    <Route component={Error404} />
                </Switch>
            </Router>
        </Provider>
    );
};

ReactDOM.render(<Main />, document.getElementById("root"));

// Opt-in to Webpack hot module replacement
if (module.hot) {
    module.hot.accept();
}
