import * as React from "react";
import Assets from "../../config/Assets";
import ImgWrapper from "../img_wrapper/ImgWrapper";
import PubMedLink from "../pubmed_link/PubMedLink";
import "./ArtificialPolarityProfiles.scss";

const ArtificialPolarityProfiles: React.FC = () => {
    return (
        <div className="polarity">
            <h1 className="page-header">Artificial Membranes Polarity Profiles</h1>
            <div className="polarity-fig">
                <ImgWrapper src={Assets.image_asset("artificialpolfig1.jpg")} />
            </div>
            <p>
                <b>Figure 1.</b> Polarity profiles of DOPC (<b>A</b>) and POPG (<b>B</b>) bilayers.
                Distributions of lipid segments determined by the simultaneous analysis of X-ray and
                neutron scattering data (left panels). Changes of polarity parameters along the
                membrane normal, hydrogen bonding donor (&alpha;), acceptor (&beta;) capacities and
                dipolarity/polarizability parameter (&pi;*), calculated as described [1] (right
                panels).
            </p>

            <div className="polarity-fig">
                <ImgWrapper src={Assets.image_asset("artificialpolfig2.jpg")} />
            </div>
            <p>
                <b>Figure 2.</b> Polarity profiles of DLPC (<b>A</b>), DMPC (<b>B</b>), DPPC (
                <b>C</b>), and DEPC (<b>D</b>) bilayers. Distributions of lipid segments determined
                from X-ray scattering data (left pannels). Changes of polarity parameters (&alpha;,
                &beta;, and &pi;*) along the membrane normal calculated as described [1] (right
                pannels).
            </p>

            <div className="polarity-fig">
                <ImgWrapper src={Assets.image_asset("artificialpolfig3.jpg")} />
            </div>
            <p>
                <b>Figure 3.</b> Polarity profiles of DHPC (<b>A</b>), DPhyPC (<b>B</b>) bilayers
                and of an eularyotic PM mimic, LM3 bilayer (<b>C</b>). Distributions of lipid
                segments determined from X-ray scattering data (left panels). Changes of polarity
                parameters (&alpha;, &beta;, and &pi;*) along the membrane normal calculated as
                described [1] (right pannels).
            </p>

            <div className="references">
                <h3>References</h3>
                <ol>
                    <li>
                        Pogozheva ID, Tristram-Nagle S, Mosberg HI, Lomize AL:{" "}
                        <b>Structural adaptations of proteins to different biological membranes.</b>{" "}
                        Biochim Biophys Acta 2013, <b>1828</b>(11):2592-2608.[
                        <PubMedLink id="23811361" />]
                    </li>
                </ol>
            </div>
        </div>
    );
};

export default ArtificialPolarityProfiles;
