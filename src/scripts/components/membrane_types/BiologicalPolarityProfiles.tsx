import * as React from "react";
import Assets from "../../config/Assets";
import ImgWrapper from "../img_wrapper/ImgWrapper";
import PubMedLink from "../pubmed_link/PubMedLink";
import "./ArtificialPolarityProfiles.scss";

const BiologicalPolarityProfiles: React.FC = () => {
    return (
        <div className="polarity">
            <h1 className="page-header">Biological Membranes Polarity Profiles</h1>
            <div className="polarity-fig">
                <ImgWrapper src={Assets.image_asset("naturalpolfig1.jpg")} />
            </div>
            <p>
                <b>Figure 1.</b> Profiles of polarity parameters (&alpha;, &beta;, and &pi;*) (J-L)
                were calculated from the distributions of different atom types on the lipid-exposed
                surface of transmembrane proteins with known structure (A-I), as described [1]
            </p>

            <div className="references">
                <h3>References</h3>
                <ol>
                    <li>
                        Pogozheva ID, Tristram-Nagle S, Mosberg HI, Lomize AL:{" "}
                        <b>Structural adaptations of proteins to different biological membranes.</b>{" "}
                        Biochim Biophys Acta 2013, <b>1828</b>(11):2592-2608.[
                        <PubMedLink id="23811361" />]
                    </li>
                </ol>
            </div>
        </div>
    );
};

export default BiologicalPolarityProfiles;
