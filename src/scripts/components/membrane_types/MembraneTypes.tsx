import * as React from "react";
import ArtificialPolarityProfiles from "./ArtificialPolarityProfiles";
import BiologicalMembraneDescription from "./BiologicalMembraneDescription";
import BiologicalPolarityProfiles from "./BiologicalPolarityProfiles";

type MembraneTypeSubsections =
    | "artificial_profiles"
    | "biological_profiles"
    | "biological_descriptions";

type MembraneTypeProps = {
    subsection: MembraneTypeSubsections;
};

const MembraneTypes: React.FC<MembraneTypeProps> = ({ subsection }) => {
    let content;
    switch (subsection) {
        case "artificial_profiles":
            content = <ArtificialPolarityProfiles />;
            break;
        case "biological_profiles":
            content = <BiologicalPolarityProfiles />;
            break;
        case "biological_descriptions":
            content = <BiologicalMembraneDescription />;
            break;
        default:
            content = "error";
            break;
    }
    return <div className="membrane-types">{content}</div>;
};

export default MembraneTypes;
