import * as React from "react";
import Assets from "../../config/Assets";
import "./LoadingImage.scss";

type LoadingImageProps = {
    small?: boolean;
    className?: string;
};

const LoadingImage: React.FC<LoadingImageProps> = ({ small, className }) => {
    return (
        <div className={className}>
            <img
                className="loading-image"
                src={small ? Assets.images.loading_image_small : Assets.images.loading_image}
                alt="Loading"
            />
        </div>
    );
};

export default LoadingImage;
