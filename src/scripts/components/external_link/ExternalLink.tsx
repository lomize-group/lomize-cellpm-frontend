import * as React from "react";
import "./ExternalLink.scss";

type ExternalLinkProps = {
    href: string;
};

const ExternalLink: React.FC<ExternalLinkProps> = ({ href, children }) => {
    return (
        <a className="external-link" target="_blank" href={href}>
            {children}
        </a>
    );
};

export default ExternalLink;
