import * as React from "react";
import Equation from "../equation/Equation";
import ExternalLink from "../external_link/ExternalLink";
import PubMedLink from "../pubmed_link/PubMedLink";
import "./About.scss";

export const About: React.FC = () => {
    return (
        <div className="about">
            <h1 className="page-header">Method</h1>
            <p>
                The method is based on calculating transfer energy <i>&Delta;G(z)</i> of a peptide
                from water to different positions within the lipid bilayer by the{" "}
                <ExternalLink href="https://opm.phar.umich.edu/ppm_server">PPM</ExternalLink> 2.0
                method using the implicit solvent model of the DOPC bilayer (PMID:21438606{" "}
                <PubMedLink id="21438606" />
                ).
            </p>
            <p>
                The method either uses the predefined structure(s) of a peptide or predicts the
                formation of membrane-bound alpha-helix similarly to the{" "}
                <ExternalLink href="https://membranome.org/fmap">FMAP</ExternalLink> method. The
                overall membrane resistance (<i>R</i>), which is inverse to the permeability
                coefficient (
                <i>
                    P<sub>m</sub>
                </i>
                ), is calculated based on the inhomogeneous solubility-diffusion model as the
                integral of the local resistance across the membrane:
            </p>
            <Equation
                className="equation"
                math={"`R = 1/P_m = int_(-d//2)^(d//2) dz/(K(z)D(z))`"}
            />
            <p>
                where <i>K(z)</i> and <i>D(z)</i> are partition and diffusion coefficients,
                respectively, which depend on the <i>z</i> position of the molecule along the
                bilayer normal; and <i>d</i> is the membrane thickness.
            </p>
            <p>
                The <i>K(z)</i> value is calculated from the Gibbs free energy of the molecule in
                membrane:
            </p>
            <Equation
                className="equation"
                math={"`K(z) = e^(-Delta G_(trans f) (z) \\ // \\ RT)`"}
            />
            <p>
                where{" "}
                <i>
                    &Delta;G<sub>transf</sub>(z)
                </i>{" "}
                is the transfer energy of the molecule from water to the position <i>z</i> along the
                bilayer normal.
            </p>
            <p>
                The server calculates the optimal translocation pathway of the peptide across the
                lipid bilayer that is represented by the{" "}
                <i>
                    &Delta;G<sub>transf</sub>(z)
                </i>{" "}
                curve. The profile{" "}
                <i>
                    &Delta;G<sub>transf</sub>(z)
                </i>{" "}
                reflects the peptide affinity to different membrane regions, while it adopts an
                optimal rotational orientation along the translocation pathway.
            </p>
            <p>
                Current version of the server is only applicable to DOPC bilayer, while including
                penalty for membrane thinning and curvature change. Future versions of the server
                will include different membranes, allow analysis of pores, and predict formation of
                &beta;-sheets.
            </p>
            <p>The calculations usually take a few minutes.</p>
        </div>
    );
};

export default About;
