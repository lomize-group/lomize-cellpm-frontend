import * as React from "react";
import "./Header.scss";
import NavBar, { NavBarItem } from "./nav_bar/NavBar";

const navItems: readonly NavBarItem[] = [
    {
        name: "HOME",
        route: "/",
    },
    {
        name: "ABOUT METHOD",
        route: "/about",
    },
    {
        name: "CELLPM",
        route: "/cellpm_server_cgopm",
    },
    {
        name: "HELP",
        route: "/help",
    },
    {
        name: "MEMBRANE TYPES",
        route: "/membrane_types",
        menuItems: [
            {
                name: "Artificial Membranes",
            },
            {
                name: "Polarity Profiles",
                route: "/membrane_types/artificial_polarity_profiles",
            },
            {
                name: "Biological Membranes",
            },
            {
                name: "Polarity Profiles",
                route: "/membrane_types/biological_polarity_profiles",
            },
            {
                name: "Description",
                route: "/membrane_types/biological_descriptions",
            },
        ],
    },
    {
        name: "CONTACT US",
        route: "/contact",
    },
    {
        name: "ACKNOWLEDGMENTS",
        route: "/acknowledgments",
    },
] as const;

const Header: React.FC = () => {
    return (
        <div className="header">
            <img className="header-img" src="/images/cellpm.jpg" alt="CellPM header image" />
            <NavBar items={navItems} />
        </div>
    );
};

export default Header;
