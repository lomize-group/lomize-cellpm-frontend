import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { RootState } from "../../../store";
import "./NavBar.scss";

function mapStateToProps(state: RootState) {
    return {
        url: state.currentUrl.url,
    };
}
const connector = connect(mapStateToProps);

export type BasicNavbarItem = {
    name: string;
    route?: string;
};

export type NavBarItem = BasicNavbarItem & {
    menuItems?: BasicNavbarItem[] | readonly BasicNavbarItem[];
};

type NavBarProps = ConnectedProps<typeof connector> & {
    items: NavBarItem[] | readonly NavBarItem[];
};

class NavBar extends React.Component<NavBarProps> {
    renderMenuItems(navItem: NavBarItem) {
        if (!navItem.menuItems) {
            return null;
        }

        const items = [];
        for (let i = 0; i < navItem.menuItems.length; ++i) {
            const menu_item = navItem.menuItems[i];

            // check if the menu item is a link or just text
            if (!menu_item.route) {
                items.push(
                    <div key={i} className="nav-bar-item text-only">
                        <strong key={i}>{menu_item.name}</strong>
                    </div>
                );
            }

            // not a link, just text
            else {
                let classes = "nav-bar-item";
                if (this.props.url === menu_item.route) {
                    classes += " selected";
                }

                items.push(
                    <div key={i} className={classes}>
                        <Link to={menu_item.route}>
                            <strong>{menu_item.name}</strong>
                        </Link>
                    </div>
                );
            }
        }
        return <div className="dropdown-list">{items}</div>;
    }

    renderNavItem(key: string | number, navItem: NavBarItem) {
        // check if it's a dropdown
        if (navItem.menuItems) {
            return (
                <div key={key} className="nav-bar-item dropdown">
                    <strong style={{ cursor: "pointer" }}>{navItem.name}</strong>
                    {this.renderMenuItems(navItem)}
                </div>
            );
        }

        // check if it's a nav link to the current page
        if (this.props.url === navItem.route) {
            return (
                <div key={key} className="nav-bar-item selected">
                    <strong>{navItem.name}</strong>
                </div>
            );
        }

        if (!navItem.route) {
            return null;
        }

        // not dropdown or current page; will be a normal link
        return (
            <div key={key} className="nav-bar-item">
                <Link to={navItem.route}>
                    <strong>{navItem.name}</strong>
                </Link>
            </div>
        );
    }

    renderNavbar() {
        let navItems = [];
        for (let i = 0; i < this.props.items.length; ++i) {
            const item = this.props.items[i];
            navItems.push(this.renderNavItem(i, item));
        }
        return navItems;
    }

    render() {
        return <div className="nav-bar">{this.renderNavbar()}</div>;
    }
}

export default connector(NavBar);
