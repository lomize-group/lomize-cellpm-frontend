import * as React from "react";
import { Link } from "react-router-dom";
import Assets from "../../config/Assets";
import "./Sidebar.scss";

const Sidebar: React.FC = () => {
    return (
        <div className="cellpm-sidebar">
            <div className="server-link">
                <Link to="/cellpm_server_cgopm">
                    <img className="server-banner" src={Assets.images.cellpm_server} />
                </Link>
            </div>
        </div>
    );
};

export default Sidebar;
