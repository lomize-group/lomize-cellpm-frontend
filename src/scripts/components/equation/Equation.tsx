import { Properties as CSSProperties } from "csstype";
import * as React from "react";
import MathJax from "react-mathjax-preview";

type EquationProps = {
    className?: string;
    style?: CSSProperties;
    math: string;
};

const Equation: React.FC<EquationProps> = (props) => {
    return (
        <div className={props.className} style={props.style}>
            <MathJax math={props.math} />
        </div>
    );
};

export default Equation;
