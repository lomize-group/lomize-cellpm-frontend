import * as React from "react";
import { useTypedStore } from "../../hooks/index";
import GLMOL from "../glmol/GLMOL.js";
import "./MoleculeViewer.scss";

const MoleculeViewer: React.FC = () => {
    const query = useTypedStore((state) => state.currentUrl.query);
    return (
        <div className="molecule-viewer">
            <h1 className="page-header">GLmol Model Viewer</h1>
            <GLMOL pdb_url={query.url} />
        </div>
    );
};

export default MoleculeViewer;
