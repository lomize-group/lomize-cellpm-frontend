import * as React from "react";
import Assets from "../../config/Assets";
import ImgWrapper from "../img_wrapper/ImgWrapper";
import "./Contact.scss";

const Contact: React.FC = () => {
    return (
        <div className="contact">
            <h1 className="page-header">Contact Us</h1>
            <p>Contact us for any CELLPM related questions.</p>
            <br />
            <div className="contact-container">
                <div className="centered contact-sect">
                    <ImgWrapper src={Assets.images.andrei} className="contact-img" />
                    <p className="contact-info centered name">
                        <a
                            href="https://pharmacy.umich.edu/lomize-group"
                            target="_blank"
                        >
                            Andrei L. Lomize
                        </a>
                        , PhD
                    </p>
                    <p className="contact-info centered role">Research Scientist</p>

                    <p className="contact-info centered actual-contact-info">
                        734-925-2370 (Office)
                        <br />
                        almz at umich.edu
                    </p>

                    <p className="contact-info centered address">
                        Room 4056
                        <br />
                        College of Pharmacy
                        <br />
                        University of Michigan
                        <br />
                        Ann Arbor, MI 48109, USA
                    </p>
                </div>
                {/* <div className="col-sm-3 centered contact-sect">
                    <ImgWrapper src={Assets.images.irina} className="contact-img" />
                    <p className="contact-info centered name">
                        <a
                            href="http://mosberglab.phar.umich.edu/people/IrinaDPogozheva.php"
                            target="_blank"
                        >
                            Irina D. Pogozheva
                        </a>
                        , PhD
                    </p>
                    <p className="contact-info centered role">Associate Research Scientist</p>
                    <p className="contact-info centered actual-contact-info">
                        734-647-5825 (Office)
                        <br />
                        irinap at umich.edu
                    </p>

                    <p className="contact-info centered address">
                        Room 3548 C.C. Little
                        <br />
                        College of Pharmacy
                        <br />
                        University of Michigan
                        <br />
                        Ann Arbor, MI 48109, USA
                    </p>
                </div> */}
                <div className="col-sm-3 centered contact-sect">
                    <ImgWrapper src={Assets.images.alexey} className="contact-img" />
                    <p className="contact-info centered name">
                        <a
                            href="https://www.linkedin.com/in/alexey-kovalenko-3455a01b6/"
                            target="_blank"
                        >
                            Alexey Kovalenko
                        </a>
                        <br />
                        B.S. in Computer Science and Biochemistry
                    </p>
                    <p className="contact-info centered role">Software Engineer</p>
                    <p className="contact-info centered actual-contact-info">
                        424-217-9712
                        <br />
                        alexeyk at umich.edu
                    </p>

                    <p className="contact-info centered address">
                        <br />
                        College of Literature, Science, and the Arts
                        <br />
                        University of Michigan
                        <br />
                        Ann Arbor, MI 48109, USA
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Contact;
