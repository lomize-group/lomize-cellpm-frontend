import * as React from "react";
import ExternalLink from "../external_link/ExternalLink";

const Acknowledgments: React.FC = () => {
    return (
        <div className="acknowledgments">
            <h1 className="page-header">Acknowledgments</h1>
            <br />
            <ul>
                {/* <li>
                    <ExternalLink href="https://www.nsf.gov/awardsearch/showAward?AWD_ID=1458002">
                        CellPM was funded by the National Science Foundation (NSF)
                    </ExternalLink>
                </li> */}
                <li>
                    CellPM was developed using NACCESS, a program for calculating solvent accessible
                    surface areas of proteins, Hubbard S.J., Thornton J.M. (1993), Department of
                    Biochemistry and Molecular Biology , University College London, UK.
                </li>
            </ul>
        </div>
    );
};

export default Acknowledgments;
