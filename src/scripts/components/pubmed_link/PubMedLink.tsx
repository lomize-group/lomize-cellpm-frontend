import * as React from "react";
import ExternalLink from "../external_link/ExternalLink";

const PUBMED_BASE_URL = "https://www.ncbi.nlm.nih.gov/pubmed/";

type PubMedLinkProps = {
    id: string;
};

const PubMedLink: React.FC<PubMedLinkProps> = ({ id, children = "PubMed" }) => {
    return <ExternalLink href={PUBMED_BASE_URL + id}>{children}</ExternalLink>;
};

export default PubMedLink;
