import * as React from "react";
import ExternalLink from "../external_link/ExternalLink";
import "./Footer.scss";

const Footer: React.FC = () => {
    return (
        <div className="footer">
            <div className="maintext">
                &copy; 2019 The Regents of the University of Michigan |{" "}
                <ExternalLink href="https://creativecommons.org/licenses/by/3.0/">
                    License
                </ExternalLink>
            </div>
        </div>
    );
};

export default Footer;
