import * as React from "react";
import ExternalLink from "../external_link/ExternalLink";
import "./Help.scss";

const Help: React.FC = () => {
    return (
        <div className="help-page">
            <h1 className="page-header">CELLPM Instructions</h1>
            <div className="page-divider">
                <h1>Input</h1>
            </div>
            <p className="info">
                The user has <u>two options to upload data</u>:
            </p>
            <ol>
                <li>
                    <b>Paste Amino Acid Sequence.</b> Paste an amino acid sequence of a peptide
                    (single-letter code, FASTA format) that will be used for the output. Avoid
                    spaces, periods or special symbols in the name.
                </li>
                <li>
                    <b>Upload PDB File.</b> Upload one or several predefined 3D models of the
                    peptide in the PDB format.
                </li>
            </ol>
            <p className="info">
                The server allows selection of <u>the membrane type</u> (only DOPC bilayer in
                current version), and choice of experimental conditions (temperature, and pH).
            </p>

            <div className="page-divider">
                <h1>Output Information</h1>
            </div>
            <p className="info">Output includes the following data:</p>
            <ol>
                <li>
                    Graphical representation of the calculated transfer energy curve,{" "}
                    <i>
                        &Delta;G<sub>transf</sub>(z)
                    </i>
                    .
                </li>
                <li>Log of calculated permeability coefficient.</li>
                <li>
                    Interactive 3D visual images of a given peptide along the translocation pathway
                    (link to GLMol) and at the lowest energy arrangement in membrane (link to JMol).
                </li>
                <li>
                    Two downloadable coordinate files: (a) first file (“XXX.pdb”) represents the
                    lowest energy arrangement of peptide in membrane; (b) second file (XXXout.pdb”)
                    represents different spatial positions of a peptide moving across the lipid
                    bilayer.
                </li>
                <li>
                    Output messages specify calculated parameters of a membrane-bound peptide:
                    position of a predicted alpha-helix in amino acid sequence, alpha-helix
                    stability, its membrane penetration depth, tilt angle, and other information.
                </li>
            </ol>

            <div className="page-divider">
                <h1>Interpretation of Results</h1>
            </div>
            <ol>
                <li>
                    Values of logP<sub>calc</sub> > −5 indicate that the peptide may readily
                    penetrate across the lipid bilayer.
                </li>
                <li>
                    GLMol link provides the interactive 3D images of a peptide moving across the
                    membrane. An additional visualization can be obtained via PyMOL open source
                    system (<ExternalLink href="https://pymol.org">PyMOL</ExternalLink>) using “load
                    XXXout.pdb, multiplex=1” command for the second output coordinate file.
                </li>
            </ol>
        </div>
    );
};

export default Help;
