import * as React from "react";
import { Link } from "react-router-dom";
import { useTypedStore } from "../../../hooks";
import ExternalLink from "../../external_link/ExternalLink";

type SubmittedProps = {
    submitAgain: () => void;
    userEmail?: string;
};

const Submitted: React.FC<SubmittedProps> = (props) => {
    const { query, url } = useTypedStore((state) => state.currentUrl);
    const waitingUrl: string | undefined = query.waitingUrl;
    const submitAgainUrl = url.replace("/submitted", "");

    if (!waitingUrl) {
        return (
            <div className="submitted-text">Must provide waitingUrl parameter in URL query.</div>
        );
    }

    return (
        <div className="submit-success dark">
            <p>
                Access your <ExternalLink href={waitingUrl}>Results</ExternalLink>
            </p>
            {props.userEmail && (
                <p>
                    You will be notified at{" "}
                    <a href={"mailto:" + props.userEmail}>{props.userEmail}</a> when the
                    calculations are complete.
                </p>
            )}
            <br />
            <div className="form-buttons">
                <Link to={submitAgainUrl} className="expand-link" onClick={props.submitAgain}>
                    <button>Submit again</button>
                </Link>
            </div>
        </div>
    );
};
export default Submitted;
