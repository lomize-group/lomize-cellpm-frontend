import axios from "axios";
import * as buildUrl from "build-url";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import Assets, { ComputationServer} from "../../config/Assets";
import { RootState } from "../../store";
import { validate } from "../../Util";
import LoadingImage from "../loading_image/LoadingImage";
import "./CellPMServer.scss";
import Submitted from "./components/Submitted";

function mapStateToProps(state: RootState) {
    return {
        currentUrl: state.currentUrl,
    };
}

const connector = connect(mapStateToProps);

type InputElems = HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement;

type MembraneType = "DOPC";
type InputMode = "paste" | "upload";

type CellPMForm = {
    sequence: string;
    pdbFile?: File;
    membraneType: MembraneType;
    temperature: number;
    ph: number;
    outputFilename: string;
    userEmail: string;
};

type CellPMServerProps = ConnectedProps<typeof connector> & {
    computationServer: ComputationServer,
    submitted?: boolean;
};

type CellPMServerState = CellPMForm & {
    loading: boolean;
    errFlag: boolean;
    errorText: string;
    waitingUrl: string;
    inputMode: InputMode;
};

function getInitialState(): CellPMServerState {
    return {
        loading: false,
        errFlag: false,
        errorText: "",
        waitingUrl: "",
        inputMode: "paste",
        sequence: "",
        pdbFile: undefined,
        membraneType: "DOPC",
        temperature: 300,
        ph: 7.0,
        outputFilename: "",
        userEmail: "",
    };
}

class CellPMServer extends React.Component<CellPMServerProps, CellPMServerState> {
    private formColSpan = 2;

    state = getInitialState();

    submit = () => {
        const { computationServer } = this.props;
        let errors = [];

        if (this.state.inputMode === "paste") {
            if (!this.state.sequence) {
                errors.push("you must enter a valid amino acid sequence");
            }
            if (!this.state.outputFilename || this.state.outputFilename === "") {
                errors.push("you must enter an output filename");
            }
        } else if (!this.state.pdbFile) {
            errors.push("you must upload a PDB file");
        }

        if (!this.state.membraneType) {
            errors.push("you must select a valid membrane type");
        }
        if (!this.state.temperature || this.state.temperature < 0) {
            errors.push("you must enter a valid temperature in Kelvin");
        }
        if (!this.state.ph || this.state.ph < 0 || this.state.ph > 14) {
            errors.push("you must enter a valid pH level");
        }
        if (!validate.alphanumeric(this.state.outputFilename)) {
            errors.push("filename must be alphanumeric");
        }
        if (!validate.email(this.state.userEmail)) {
            errors.push("the email entered is invalid");
        }

        const errFlag = errors.length > 0;
        this.setState({
            errFlag,
            errorText: errors.join("; "),
        });

        if (errFlag) {
            return;
        }

        let data = new FormData();
        data.append("outputPdbFilename", this.state.outputFilename);
        data.append("membraneType", this.state.membraneType);
        data.append("temperature", this.state.temperature.toString());
        data.append("ph", this.state.ph.toString());
        data.append("userEmail", this.state.userEmail);

        if (this.state.inputMode === "upload") {
            data.append("pdbFile", this.state.pdbFile!);
        } else {
            data.append("sequence", this.state.sequence);
        }

        axios
            .post(Assets.computation_servers[computationServer]  + "cellpm", data)
            .then((res) => {
                const currentUrl = this.props.currentUrl.url;
                const submitted = currentUrl.charAt(currentUrl.length - 1) === '/' ? "submitted" : "/submitted"
                const submitUrl = buildUrl(currentUrl + submitted, {
                    queryParams: {
                        waitingUrl: res.data.waitingUrl,
                    },
                });
                this.props.currentUrl.history.push(submitUrl);
                this.setState({ loading: false });
            })
            .catch((err) => {
                this.setState({
                    errFlag: true,
                    errorText: "An error occurred: " + JSON.stringify(err),
                    loading: false,
                });
            });

        this.setState({ loading: true });
    };

    reset = () => {
        this.setState(getInitialState());
    };

    onFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const { files } = e.target;
        const pdbFile = files && files.length > 0 ? files[0] : undefined;
        this.setState({ pdbFile });
    };

    changeHandler = (key: keyof CellPMForm) => (e: React.ChangeEvent<InputElems>) => {
        this.setState({ [key]: (e.target as HTMLInputElement).value } as Pick<
            CellPMServerState,
            any
        >);
    };

    renderFormButtons() {
        return (
            <div className="form-buttons">
                <button onClick={this.submit}>
                    <u>S</u>ubmit
                </button>
                <button onClick={this.reset}>
                    <u>R</u>eset
                </button>{" "}
                <span className="error-message">{this.state.errorText}</span>
            </div>
        );
    }

    renderInputSection() {
        return (
            <tr className="input-section dark">
                <td colSpan={this.formColSpan}>
                    <div className="tab-buttons">
                        <table className="inline-table">
                            <tbody>
                                <tr>
                                    <td className="centered">
                                        <button
                                            name="paste"
                                            className={
                                                this.state.inputMode === "paste" ? "selected" : ""
                                            }
                                            onClick={() => this.setState({ inputMode: "paste" })}
                                        >
                                            Paste Amino Acid Sequence
                                        </button>
                                        <br />
                                        <span className="subtext centered">FASTA format</span>
                                    </td>
                                    <td className="centered">
                                        <button
                                            name="upload"
                                            className={
                                                this.state.inputMode === "upload" ? "selected" : ""
                                            }
                                            onClick={() => this.setState({ inputMode: "upload" })}
                                        >
                                            Upload 3D structure
                                        </button>
                                        <br />
                                        <span className="subtext centered">PDB format</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    {this.state.inputMode === "paste" ? (
                        <div className="user-input-container">
                            <label htmlFor="sequence" className="subtext">
                                Amino acid sequence (one-letter code)
                            </label>
                            <textarea
                                id="sequence"
                                name="sequence"
                                className="text-input"
                                placeholder="Enter your text here"
                                value={this.state.sequence}
                                onChange={this.changeHandler("sequence")}
                            />
                        </div>
                    ) : (
                        <div className="user-input-container">
                            <input
                                id="pdbFile"
                                type="file"
                                className="file-input"
                                name="pdbFile"
                                size={7000000}
                                onChange={this.onFileChange}
                            />
                        </div>
                    )}
                </td>
            </tr>
        );
    }

    renderOptionsSection() {
        return (
            <tr>
                <td className="options-section">
                    <div className="input-subsection">
                        <label htmlFor="membraneType" className="title">
                            Membrane type:{" "}
                        </label>
                        <select
                            value={this.state.membraneType}
                            id="membraneType"
                            name="membraneType"
                            onChange={this.changeHandler("membraneType")}
                        >
                            <option value="DOPC">DOPC</option>
                        </select>
                    </div>
                </td>
                <td className="options-section">
                    <div>
                        <div className="title">
                            Experimental conditions:{" "}
                            <span className="subtext">
                                (default values depend on membrane type)
                            </span>
                        </div>
                        <table className="inline-table">
                            <tbody>
                                <tr>
                                    <td>
                                        <label htmlFor="temperature" className="subtext">
                                            T&deg; (K)
                                        </label>
                                        <br />
                                        <input
                                            id="temperature"
                                            name="temperature"
                                            type="number"
                                            min={0}
                                            placeholder="Temperature K"
                                            value={this.state.temperature}
                                            onChange={this.changeHandler("temperature")}
                                        />
                                    </td>
                                    <td>
                                        <label htmlFor="ph" className="subtext">
                                            pH
                                        </label>
                                        <br />
                                        <input
                                            id="ph"
                                            name="ph"
                                            type="number"
                                            min={0}
                                            max={14}
                                            step={0.1}
                                            placeholder="pH"
                                            value={this.state.ph}
                                            onChange={this.changeHandler("ph")}
                                        />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
            </tr>
        );
    }

    renderOutputSection() {
        return (
            <tr>
                <td colSpan={this.formColSpan}>
                    <div className="title">The server calculates and provides:</div>
                    <ul className="output-info-list">
                        <li>Predicted alpha-helix and its stability in membrane</li>
                        <li>Permeability coefficient</li>
                        <li>
                            Graphical representation of the transmembrane translocation pathway (
                            <i>&Delta;G(z)</i> curve)
                        </li>
                        <li>
                            Interactive 3D images of a peptide along the translocation pathway (with
                            GLmol and Jmol)
                        </li>
                        <li>
                            Downloadable coordinate files of the membrane bound state and multiple
                            structures along the translocation pathway (in pdb format)
                        </li>
                    </ul>
                    <div
                        className={this.state.inputMode === "paste" ? "input-subsection" : "hidden"}
                    >
                        <label htmlFor="outputFilename" className="title">
                            Name of the output PDB file:
                        </label>
                        <br />
                        <input
                            id="outputFilename"
                            name="outputFilename"
                            type="text"
                            value={this.state.outputFilename}
                            onChange={this.changeHandler("outputFilename")}
                        />{" "}
                        <span className="subtext">
                            ex. abcd (.pdb extension will be added automatically)
                        </span>
                    </div>
                    <label htmlFor="userEmail" className="title">
                        User Email (optional):
                    </label>
                    <div className="input-subsection">
                        <input
                            id="userEmail"
                            name="userEmail"
                            type="text"
                            value={this.state.userEmail}
                            onChange={this.changeHandler("userEmail")}
                        />{" "}
                        <span className="subtext">
                            (this will send you an email with your results)
                        </span>
                    </div>
                </td>
            </tr>
        );
    }

    renderContent() {
        const { computationServer } = this.props;
        if (this.props.submitted) {
            return <Submitted submitAgain={this.reset} userEmail={this.state.userEmail} />;
        }

        const useOtherServer = computationServer == "memprot"
            ? <div className="use-old-server">
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/cellpm_server_cgopm">cgopm-based</Link> version
            </div>
            : <div className="use-old-server">
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/cellpm_server">AWS-based</Link> version
            </div>;

        return (
            <>
                <p>
                    The CELLPM server is a physics-based computational tool for analysis of
                    peptide-membrane interactions and prediction of peptide ability to cross the
                    lipid bilayer via an energy-independent pathway. It can use an amino acid
                    sequence or a predefined 3D structure of the peptide to calculate its optimal
                    spatial position in membrane, the energy of membrane binding, the lowest energy
                    translocation pathway across the lipid bilayer, and the permeability
                    coefficient.
                </p>
                <p>
                    First option of the server uses the amino acid sequence of a peptide as input.
                    It is aimed at calculation of transmembrane translocation pathways and
                    permeability coefficients of peptides that are usually unfolded in the aqueous
                    solution, but form alpha-helices during membrane binding.
                </p>
                <p>
                    Second option allows visualization of passive translocation pathway for peptides
                    with stable 3D structures in water that do not change significantly during the
                    translocation. The 3D structures should be experimentally determined or
                    computationally modeled. See <Link to="/help">instructions</Link> for details.
                </p>

                <div className="cellpm-form">
                    {this.renderFormButtons()}

                    <table className="cellpm-form-table">
                        <tbody>
                            <tr className="page-divider">
                                <td colSpan={this.formColSpan} className="form-section-header">
                                    <b>Input</b>
                                </td>
                            </tr>
                            {this.renderInputSection()}

                            <tr className="page-divider">
                                <td colSpan={this.formColSpan} className="form-section-header">
                                    <b>Options</b>
                                </td>
                            </tr>
                            {this.renderOptionsSection()}

                            <tr className="page-divider">
                                <td colSpan={this.formColSpan} className="form-section-header">
                                    <b>Output</b>
                                </td>
                            </tr>
                            {this.renderOutputSection()}
                        </tbody>
                    </table>
                    {this.renderFormButtons()}
                </div>
                {useOtherServer}
            </>
        );
    }

    render() {
        const { computationServer } = this.props;
        if (this.state.loading) {
            return <LoadingImage />;
        }

        const serverType = computationServer == "memprot"
            ? "AWS"
            : computationServer;

        return (
            <div className="cellpm-server">
                <h1 className="page-header">CELLPM Server ({serverType})</h1>
                {this.renderContent()}
            </div>
        );
    }
}

export default connector(CellPMServer);
