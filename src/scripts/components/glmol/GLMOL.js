import axios from "axios";
import * as PropTypes from "prop-types";
import * as React from "react";
import scriptLoader from "react-async-script-loader"; // need to load scripts for glmol
import LoadingImage from "../loading_image/LoadingImage";
import "./GLMOL.scss";

class GLMOL extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            models: undefined,
            loaded: false,
            model_number: 0,
            src: "",
        };

        // doesn't need to be part of state because it doesn't need to trigger updates
        this.glmol_obj = undefined;

        this.change_model = (new_model_num) => {
            // keep within bounds of available models
            if (new_model_num >= this.state.models.length) {
                new_model_num = this.state.models.length - 1; // keep at the final model
            }
            if (new_model_num < 0) {
                new_model_num = 0; // keep at the first model
            }

            // don't need to update if the model did not change
            if (new_model_num == this.state.model_number) {
                return;
            }

            // update current model and source
            this.setState(
                {
                    model_number: new_model_num,
                    src: this.state.models[new_model_num].source.join("\n"),
                },
                () => {
                    // re-render glmol scene for setState callback
                    this.glmol_obj.rebuildScene();
                    this.glmol_obj.loadMolecule();
                }
            );
        };

        this.parse_data = (data) => {
            const lines = data.split("\n");
            let cur_model_number = 0;
            let models = [{ source: [[" "]] }];

            for (let i = 0; i < lines.length; ++i) {
                const line = lines[i];
                models[cur_model_number].source.push([line]);

                // indicate start of model
                if (line.startsWith("MODEL")) {
                    // get the model number
                    const model_number = line.trim().replace(/^\D+/g, "");
                    models[cur_model_number].model_number = model_number;
                }
                // record data from REMARK lines
                else if (line.startsWith("REMARK")) {
                    if (line.startsWith("REMARK  Z=")) {
                        const remarks = line.substring(6, line.length); // 6 is length of REMARK
                        const remarkArray = remarks.replace(/ /g, "").split(","); //remove commas and spit by space

                        // get the data of the remark and record in the model
                        for (let j = 0; j < remarkArray.length; ++j) {
                            const propertyArray = remarkArray[j].split("="); // properties of the remark (e.g., Z and Energy)
                            models[cur_model_number][propertyArray[0]] = parseFloat(
                                propertyArray[1]
                            );
                        }
                    }
                }
                // end of model so push another object for the next model
                else if (line.startsWith("ENDMDL")) {
                    cur_model_number += 1;
                    models.push({ source: [[" "]] });
                }
            }

            // the final model will be empty from the loop
            models.pop();
            const src = models.length === 0 ? data : models[0].source.join("\n");

            // update state and trigger re-render
            this.setState(
                {
                    models: models,
                    src: src, // default to first model
                    loaded: true,
                },
                () => {
                    // render glmol scene for callback
                    this.glmol_obj.loadMolecule();
                }
            );
        };

        this.init = () => {
            // create glmol object
            this.glmol_obj = new GLmol("glmol", true);

            // representation definition needed by glmol
            this.glmol_obj.defineRepresentation = function () {
                let all = this.getAllAtoms();
                let allHet = this.getHetatms(all);
                let hetatm = this.removeSolvents(allHet);

                // coloring
                this.colorByAtom(all, {});
                this.colorByChain(all);
                this.colorByResidue(this.getSidechains(all), this.AminoAcidResidueColors);

                let asu = new THREE.Object3D();

                this.drawAtomsAsSphere(asu, hetatm, this.sphereRadius);
                this.drawBondsAsStick(
                    asu,
                    this.getSidechains(all),
                    this.cylinderRadius,
                    this.cylinderRadius
                );
                this.drawCartoon(asu, all, this.curveWidth, this.thickness);
                this.modelGroup.add(asu);
            };

            // fetch pdb file
            axios.get(this.props.pdb_url).then((res) => {
                this.parse_data(res.data);
            });
        };
    }

    renderRemarksTable() {
        // loading image while data is loading and parsing
        if (!this.state.loaded) {
            return (
                <div className="model-table">
                    <LoadingImage className="table-loading" />
                </div>
            );
        }

        // don't render a table if there are not multiple models
        if (!this.state.models || this.state.models.length === 0) {
            return null;
        }

        // generate the table rows for each model
        const rows = [];
        for (let i = 0; i < this.state.models.length; ++i) {
            const model = this.state.models[i];
            rows.push(
                <tr
                    key={i}
                    className={i === this.state.model_number ? "selected-row" : ""}
                    onClick={() => this.change_model(i)}
                >
                    <td>{model.model_number}</td>
                    <td>{model.Z}</td>
                    <td>{model.energy}</td>
                </tr>
            );
        }

        // render the table
        return (
            <div className="model-table">
                <table id="model-table">
                    <thead>
                        <tr>
                            <th>Model</th>
                            <th>Z (A)</th>
                            <th>Energy (kcal/mol)</th>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
            </div>
        );
    }

    componentWillReceiveProps({ isScriptLoaded, isScriptLoadSucceed }) {
        // initialize when glmol scripts and dependenceis are loaded
        if (isScriptLoaded && !this.props.isScriptLoaded && isScriptLoadSucceed) {
            // load finished
            this.init(this.props.complex);
        }
    }

    componentDidMount() {
        // initialize when glmol scripts and dependenceis are loaded
        const { isScriptLoaded, isScriptLoadSucceed } = this.props;
        if (isScriptLoaded && isScriptLoadSucceed) {
            this.init(this.props.complex);
        }
    }

    render() {
        // the glmol_src textarea is important; glmol uses its value for the model to display
        return (
            <div className="glmol">
                <div className="viewer-container">
                    <div id="glmol"></div>
                    <div
                        className={
                            !this.state.models || this.state.models.length === 0
                                ? "hidden"
                                : "model-control-arrows"
                        }
                    >
                        <span
                            className="arrow"
                            onClick={() => this.change_model(this.state.model_number - 1)}
                        >
                            &lt; Previous
                        </span>
                        <span
                            className="arrow"
                            onClick={() => this.change_model(this.state.model_number + 1)}
                        >
                            Next &gt;
                        </span>
                    </div>
                </div>
                {this.renderRemarksTable()}
                <textarea id="glmol_src" className="hidden" value={this.state.src} readOnly />
            </div>
        );
    }
}

GLMOL.propTypes = {
    pdb_url: PropTypes.string, // the url that the pdb file will be loaded from
};

export default scriptLoader(
    "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js", // JQuery neede by GLmol
    "https://storage.googleapis.com/membranome-assets/server_assets/Three49custom.js", // used by GLmol
    "https://storage.googleapis.com/membranome-assets/server_assets/GLmol.js" // the GLmol script
)(GLMOL);
