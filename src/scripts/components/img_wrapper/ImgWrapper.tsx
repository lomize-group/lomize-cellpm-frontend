import * as React from "react";
import Assets from "../../config/Assets";
import "./ImgWrapper.scss";

type ImgWrapperProps = {
    src: string;
    className?: string;
    fallback?: string;
    inline?: boolean;
};

type ImgWrapperState = {
    loaded: boolean;
    found: boolean;
};

export default class ImgWrapper extends React.Component<ImgWrapperProps, ImgWrapperState> {
    state = {
        loaded: false,
        found: true,
    };

    handleLoad = () => {
        this.setState({ loaded: true });
    };
    handleError = (e: React.ChangeEvent<HTMLImageElement>) => {
        e.target.src = this.props.fallback ? this.props.fallback : Assets.images.no_image;
    };

    componentWillReceiveProps(nextProps: ImgWrapperProps) {
        if (this.props.src !== nextProps.src) {
            this.setState({
                loaded: false,
                found: true,
            });
        }
    }

    render() {
        const image = (
            <img
                className={this.state.loaded ? this.props.className : "hidden"}
                src={this.props.src}
                onLoad={this.handleLoad}
                onError={this.handleError}
            />
        );
        const loading = (
            <img
                className={this.state.loaded ? "hidden" : this.props.className}
                src={Assets.images.loading_image}
            />
        );

        if (this.props.inline) {
            return (
                <span className="img-wrapper">
                    {image}
                    {loading}
                </span>
            );
        }

        return (
            <div className="img-wrapper">
                {image}
                {loading}
            </div>
        );
    }
}
