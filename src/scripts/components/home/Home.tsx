import * as React from "react";
import Assets from "../../config/Assets";
import ExternalLink from "../external_link/ExternalLink";
import "./Home.scss";

const Home: React.FC = () => {
    return (
        <div className="home">
            <p>
                The discovery of a new class of cell-penetrating peptides (CPPs) two decade ago
                rapidly changed the general view on the inability of highly polar and charged
                molecules to enter cells without causing irreversible membrane damage.
            </p>
            <p>
                Here we provide CELLPM, a web-based tool aimed at validation of ability of peptides
                to passively penetrate across the lipid bilayer. This tool implements novel
                computational methods based on physically realistic description of different aspects
                of membrane-peptide interactions, including peptide insertion into membranes,
                formations of alpha-helices, and peptide-induced membrane thinning or curvature
                changes. This would help to understand mechanisms of direct peptide translocation
                across the lipid bilayer.
            </p>
            <div className="page-divider">
                <h1>References</h1>
            </div>
            <p>
                Lomize AL and Pogozheva ID. Modeling of peptide folding and translocation across
                membranes. Biophys J, 2018, 114(3): 267a [
                <ExternalLink href="https://www.sciencedirect.com/science/article/pii/S0006349517327777">
                    Reference
                </ExternalLink>
                ]
            </p>
            <br />
            {/* <p style={{ textAlign: "right" }}>
                <ExternalLink href="https://www.nsf.gov/awardsearch/showAward?AWD_ID=1458002">
                    CellPM was funded by the National Science Foundation (NSF)
                </ExternalLink>
                <img src={Assets.images.nsf} alt="NSF" />
            </p> */}
        </div>
    );
};

export default Home;
