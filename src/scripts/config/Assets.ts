export type ComputationServer = "memprot" | "cgopm" | "local";

const googleCloudBase = "https://storage.googleapis.com/cellpm-assets/" as const;
const imageAssetUrl = "https://storage.googleapis.com/cellpm-assets/images/image_assets/";

const Assets = {
    computation_servers: {
        memprot: "https://memprot.org/",
        cgopm: "https://cgopm.cc.lehigh.edu/",
        local: "http://localhost:9000/",
    },

    images: {
        andrei: googleCloudBase + "images/portraits/Andrei.jpg",
        irina: googleCloudBase + "images/portraits/Ira.jpg",
        alexey: googleCloudBase + "images/portraits/Alex.jpg",
        loading_image: "/images/loading.gif",
        loading_image_small: "/images/loading_small.gif",
        no_image: "/images/no_image.jpeg",
        cellpm_server: "/images/CELLPM_server1.png",
        nsf: imageAssetUrl + "nsf.gif",
    },

    image_asset: (imgName: string) => imageAssetUrl + imgName,
    pubmed: (pubmedId: string) => "https://www.ncbi.nlm.nih.gov/pubmed/" + pubmedId,
} as const;

export default Assets;
