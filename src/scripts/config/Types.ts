export type GenericObj<V = any> = { [key: string]: V };
