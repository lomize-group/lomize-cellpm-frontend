declare module "react-mathjax-preview" {
    import { Component } from "react";

    export type MathJaxProps = {
        math: string;
    };

    class MathJax extends Component<MathJaxProps> {}

    export default MathJax;
}
